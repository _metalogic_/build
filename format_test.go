package build

import (
	"fmt"
	"testing"
	"time"
)

func TestFormat(t *testing.T) {

	thu, err := time.Parse(time.RFC1123, "Thu, 23 Dec 2021 08:14:00 PST")
	if err != nil {
		t.Fatal(err)
	}

	testCases := []struct {
		Name     string
		Template string
		I        *BuildInfo
		Expected string
	}{
		{"Test 1", "<pre>((Project))\n(version ((Version)), revision ((Revision)))\nbuilt at {{ .Built ))</pre>\n\n", &BuildInfo{Project: "Test 1", Version: "develop", Revision: "abc", LastCommit: thu}, "<pre>Test 1\n(version develop, revision abc)\nbuilt at Thu, 23 Dec 2021 08:14:00 PST</pre>\n\n"},
		{"Test 2", "<pre>((Project))\n(version ((Version)), revision ((Revision)))\nbuilt at {{ .Built ))</pre>\n\n", &BuildInfo{Project: "Test 2", Version: "main", Revision: "abc", LastCommit: thu}, "<pre>Test 2\n(version main, revision abc)\nbuilt at Thu, 23 Dec 2021 08:14:00 PST</pre>\n\n"},
	}

	for _, tc := range testCases {
		t.Run(tc.Name, func(t *testing.T) {
			s, e := tc.I.Format(tc.Template)
			if e != nil {
				t.Error(e)
				return
			}

			if s != fmt.Sprintf(tc.Expected) {
				t.Errorf("%s != %s", s, fmt.Sprintf(tc.Expected))
			} else {
				t.Logf("%s == %s", s, fmt.Sprintf(tc.Expected))
			}
		})

	}

}
