package main

import (
	"encoding/json"
	"fmt"
	"log"

	"bitbucket.org/_metalogic_/build"
)

func main() {

	info := build.Info
	b, err := json.MarshalIndent(info, "", "   ")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("\n-------------\nInfo JSON:\n%s\n", string(b))

	fmt.Printf("\n-------------\ninfo.String(): %s\n", info.String())

	fmt.Printf("\n-------------\ninfo.Name(): %s\n", info.Name())
}
